package org.fundacion.jala.converter.core;

import org.fundacion.jala.converter.core.exceptions.TextExtractorException;
import org.fundacion.jala.converter.core.parameter.ExtractTextParameter;
import org.junit.Test;

import org.junit.Ignore;

public class ExtractTextTest {

    @Test(expected = TextExtractorException.class)
    public void itShouldExtractTextException() throws TextExtractorException {
        ExtractText extractText = new ExtractText("language", "newpath");
        extractText.extractText();
    }

    @Test(expected = TextExtractorException.class)
    public void itShouldExtractTextExceptionNull() throws TextExtractorException {
        ExtractText extractText = new ExtractText(null);
        extractText.extractText();
    }

    @Ignore
    @Test
    public void itShouldExtractTextWithName() throws TextExtractorException {
        ExtractText extractText = new ExtractText(new ExtractTextParameter("src//test//resources//imagenText.png", "eng", "imagenTest"));
        extractText.extractText();
    }

    @Ignore
    @Test
    public void itShouldExtractText() throws TextExtractorException {
        ExtractText extractText = new ExtractText(new ExtractTextParameter("src//test//resources//imagenText.png"));
        extractText.extractText();
    }

    @Test(expected = TextExtractorException.class)
    public void itShouldExtractTextExceptionNameOutputNull() throws TextExtractorException {
        ExtractText extractText = new ExtractText("language", "newpath", null);
        extractText.extractText();
    }

}